<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailList;
use Illuminate\Support\Str;
use Mail;
use App\Mail\SignupEmail;

class EmailListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if the email address is already registered
        $data = $request->json()->all();
        $email = EmailList::where('email', $data['email'])->count();

        if ($email > 0)
          return response()->json(['msg' => 'This email is already registered']);

        //Create a UUID
        $uuid = Str::uuid();

        // Add email to DB
        $email = new EmailList;
        $email->email = $data['email'];
        $email->key = $uuid->toString();

        // Send the email
        $resp = Mail::to($email)->send(new SignupEmail($email));
        if (!$resp) {
          $email->save();
          return response()->json(['msg' => "You're on the list!"]);
        } else {
          return response()->json(['msg' => 'There was an error ' . $resp]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Unsubscribe a user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request)
    {
        // Make sure email and key are set
        if (!isset($request->email) || !isset($request->key))
          return view('unsubscribe')->with(['success' => false]);

        // Delete the email if key matches
        $email = EmailList::where('email', $request->email)->first();

        //Check that the key matches
        if ($email->key == $request->key) {
          $email->delete();
          return view('unsubscribe')->with(['success' => true]);
        } else {
          return view('unsubscribe')->with(['success' => false]);
        }

    }
}
