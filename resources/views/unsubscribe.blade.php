<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{ csrf_token() }}">

        <title>LocoCMS | The flexible CMS package for Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Stylesheet -->
        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
      <div id="app" class="w-full">
        <unsubscribe success="{{ $success }}" email="{{ ( isset($_REQUEST['email']) ? $_REQUEST['email'] : null ) }}" emailKey="{{ ( isset($_REQUEST['key']) ? $_REQUEST['key'] : null ) }}"></unsubscribe>
      </div>
      <!-- Load Vue -->
      <script src="js/app.js"></script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156252363-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156252363-1');
      </script>

    </body>
</html>
