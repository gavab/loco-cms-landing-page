<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{ csrf_token() }}">
        <meta name="Description" content="LocoCMS - the CMS package for Laravel">

        <title>LocoCMS | The flexible CMS package for Laravel</title>

        <!-- Stylesheet -->
        <link href="css/app.css" rel="stylesheet">
    </head>
    <body>
      <div id="app" class="w-full">
        <homepage></homepage>
      </div>
      <!-- Load Vue -->
      <script src="js/app.js"></script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156252363-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156252363-1');
      </script>

    </body>
</html>
