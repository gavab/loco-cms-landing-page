<p>Hi!</p>
<p>Thanks for taking the time to check out LocoCMS and sign up for our mailing list.</p>
<p>I started creating LocoCMS to make development of Laravel projects easier, and I hope that you'll find it useful.</p>
<p>I'm excited about what the next few months will bring on this project, so stay tuned for updates!</p>
<p>Thanks,</p>
<p>Gavin</p>
<p><img src="https://lococms.net/img/email/{{ $data->key }}.png" alt="LocoCMS" /></p>
<p style="font-size: small"><a href="https://lococms.net/unsubscribe?email={{ $data->email }}&key={{ $data->key }}">Unsubscribe</a></p>
